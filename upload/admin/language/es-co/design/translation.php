<?php

$_['heading_title'] = 'Traducción de idiomas';
$_['text_edit'] = 'Editar traducción';
$_['text_list'] = 'Lista de traducciones';
$_['text_translation'] = 'traducciones';
$_['column_flag'] = 'Bandera';
$_['column_country'] = 'País';
$_['column_progress'] = 'Progreso de la traducción';
$_['column_action'] = 'Acción';
$_['button_install'] = 'Instalar';
$_['button_uninstall'] = 'Desinstalar';
$_['button_refresh'] = 'Refrescar';
$_['error_permission'] = 'Advertencia: ¡No tienes permiso para modificar la traducción!';
