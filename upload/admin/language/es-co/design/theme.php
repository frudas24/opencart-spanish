<?php

$_['heading_title'] = 'Editor de Temas';
$_['text_success'] = 'Éxito: ¡Has modificado los temas!';
$_['text_edit'] = 'Editar Tema';
$_['text_store'] = 'Elija su tienda';
$_['text_template'] = 'Elija una plantilla';
$_['text_default'] = 'Predeterminado';
$_['text_warning'] = 'Advertencia: La seguridad puede verse comprometida usando el editor de temas!';
$_['text_access'] = 'Asegúrese de que sólo los usuarios admin correctos están autorizados a acceder a esta página, ya que puede acceder directamente a su código fuente de la tienda.';
$_['text_permission'] = 'Puede cambiar los permisos de usuario <a href="%s" class="alert-link"> aquí </a>.';
$_['text_begin'] = 'Seleccione un archivo de tema del lado izquierdo para comenzar a editar.';
$_['error_permission'] = 'Advertencia: ¡No tienes permiso para modificar el editor de temas!';
