<?php

$_['heading_title'] = 'Editor de Lenguaje';
$_['text_success'] = 'Éxito: has modificado el editor de idiomas!';
$_['text_edit'] = 'Editar traducción';
$_['text_default'] = 'Predeterminado';
$_['text_store'] = 'Tienda';
$_['text_language'] = 'Idioma';
$_['text_translation'] = 'Traducciones';
$_['entry_key'] = 'Clave';
$_['entry_value'] = 'Valor';
$_['entry_default'] = 'Predeterminado';
$_['error_permission'] = 'Advertencia: ¡No tienes permiso para modificar el editor de idiomas!';
