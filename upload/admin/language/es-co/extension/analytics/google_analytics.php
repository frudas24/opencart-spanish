<?php
$_['heading_title']    = 'Google Analytics';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']	   = 'Éxito: has modificado Google Analytics!';
$_['text_signup']      = 'Inicie sesión en su cuenta de <a href="http://www.google.com/analytics/" target="_blank"> <u> Google Analytics </ u> </a> y después de crear su copia de perfil de sitio web Pegue el code Analytics en este campo.';
$_['text_default']     = 'Default';

// Entry
$_['entry_code']       = 'Google Analytics Code';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: no tienes permiso para modificar Google Analytics!';
$_['error_code']	   = 'Código requerido!';
