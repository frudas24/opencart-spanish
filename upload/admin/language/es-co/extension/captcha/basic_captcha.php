<?php
$_['heading_title']    = 'Basic Captcha';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']	   = 'Éxito: has modificado Captcha básico!';
$_['text_edit']        = 'Editar Captcha Básica';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tienes permiso para modificar Captcha básico!';
