<?php
// Heading
$_['heading_title']    = 'Ventas totales';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: has modificado las ventas del panel!';
$_['text_edit']        = 'Editar Ventas del panel';
$_['text_view']        = 'Ver más ...';

// Entry
$_['entry_status']     = 'Estados';
$_['entry_sort_order'] = 'Ordenar Pedido';
$_['entry_width']      = 'Ancho';

// Error
$_['error_permission'] = 'Advertencia: ¡No tienes permiso para modificar las ventas del panel de control!';