<?php
// Heading
$_['heading_title']    = 'Total de pedidos';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: has modificado las órdenes del panel!';
$_['text_edit']        = 'Editar órdenes del panel de mando';
$_['text_view']        = 'Ver más ...';

// Entry
$_['entry_status']     = 'Estados';
$_['entry_sort_order'] = 'Ordenar Pedido';
$_['entry_width']      = 'Ancho';

// Error
$_['error_permission'] = 'Advertencia: ¡No tienes permiso para modificar las órdenes del panel!';