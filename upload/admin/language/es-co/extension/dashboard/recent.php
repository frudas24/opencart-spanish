<?php
// Heading
$_['heading_title']     = 'Últimos pedidos';

// Text
$_['text_extension']    = 'Extensiones';
$_['text_success']      = 'Éxito: has modificado las órdenes recientes del panel de control!';
$_['text_edit']         = 'Editar órdenes recientes del panel';

// Column
$_['column_order_id']   = 'ID del Pedido';
$_['column_customer']   = 'Cliente';
$_['column_status']     = 'Estados';
$_['column_total']      = 'Total';
$_['column_date_added'] = 'Fecha agregada';
$_['column_action']     = 'Acción';


// Entry
$_['entry_status']      = 'Estado';
$_['entry_sort_order']  = 'Ordenar Pedido';
$_['entry_width']       = 'Ancho';

// Error
$_['error_permission']  = 'Advertencia: ¡No tienes permiso para modificar las órdenes recientes del Panel de mando!';