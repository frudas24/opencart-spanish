<?php
// Heading
$_['heading_title']    = 'Personas en línea';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: has modificado el panel en línea!';
$_['text_edit']        = 'Editar panel en línea';
$_['text_view']        = 'Ver más ...';


// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar Pedido';
$_['entry_width']      = 'Ancho';

// Error
$_['error_permission'] = 'Advertencia: ¡No tienes permiso para modificar el panel en línea!';