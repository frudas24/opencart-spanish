<?php
// Heading
$_['heading_title']    = 'Mapa del Mundo';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: has modificado el mapa de panel!';
$_['text_edit']        = 'Editar mapa de panel';
$_['text_order']       = 'Pedidos';
$_['text_sale']        = 'Ventas';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar Pedido';
$_['entry_width']      = 'Ancho';

// Error
$_['error_permission'] = 'Advertencia: ¡No tienes permiso para modificar el mapa del panel!';