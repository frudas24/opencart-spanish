<?php
// Heading
$_['heading_title']    = 'Análisis de ventas';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ha modificado el gráfico del panel!';
$_['text_edit']        = 'Editar graficos del panel';
$_['text_order']       = 'Pedidos';
$_['text_customer']    = 'Clientes';
$_['text_day']         = 'Hoy';
$_['text_week']        = 'Semana';
$_['text_month']       = 'Mes';
$_['text_year']        = 'Año';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar Pedido';
$_['entry_width']      = 'Ancho';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permiso para modificar el gráfico del panel! ';