<?php
// Heading
$_['heading_title']                = 'Actividad reciente';

// Text
$_['text_extension']               = 'Extensiones';
$_['text_success']                 = 'Éxito: ha modificado la actividad del panel de control!';
$_['text_edit']                    = 'Editar actividad reciente del panel';
$_['text_customer_address_add']    = '<a href="customer_id=%d">%s </a> ha añadido una nueva dirección.';
$_['text_customer_address_edit']   = '<a href="customer_id=%d">%s </a> ha actualizado su dirección. ';
$_['text_customer_address_delete'] = '<a href="customer_id=%d">%s </a> eliminó una de sus direcciones.';
$_['text_customer_edit']           = '<a href="customer_id=%d">%s </a> actualizó los detalles de su cuenta.';
$_['text_customer_forgotten']      = '<a href="customer_id=%d">%s </a> ha solicitado una contraseña de restablecimiento.';
$_['text_customer_reset']          = '<a href="customer_id=%d">%s </a> restablecer la contraseña de su cuenta.';
$_['text_customer_login']          = '<a href="customer_id=%d">%s </a> ha iniciado sesión.';
$_['text_customer_password']       = '<a href="customer_id=%d">%s </a> ha actualizado la contraseña de su cuenta.';
$_['text_customer_register']       = '<a href="customer_id=%d">%s </a> ha registrado una nueva cuenta.';
$_['text_customer_return_account'] = '<a href="customer_id=%d">%s </a> ha enviado un producto <a href="return_id=%d"> devolver </a>.';
$_['text_customer_return_guest']   = '%s ha enviado un producto <a href="return_id=%d"> devolver </a>.';
$_['text_customer_order_account']  = '<a href="customer_id=%d">%s </a> agregó un <a href="order_id=%d"> nuevo pedido </a>.';
$_['text_customer_order_guest']    = '%s created a <a href="order_id=%d">new order</a>.';
$_['text_affiliate_edit']          = '<a href="affiliate_id=%d">%s</a> Actualizaron los detalles de su cuenta.';
$_['text_affiliate_forgotten']     = '<a href="affiliate_id=%d">%s</a>Ha solicitado una nueva contraseña.';
$_['text_affiliate_login']         = '<a href="affiliate_id=%d">%s</a> conectado.';
$_['text_affiliate_password']      = '<a href="affiliate_id=%d">%s</a> Actualizada su contraseña de cuenta.';
$_['text_affiliate_payment']       = '<a href="affiliate_id=%d">%s</a> Actualizado sus detalles de pago.';
$_['text_affiliate_register']      = '<a href="affiliate_id=%d">%s</a> Registrado para una nueva cuenta.';

// Entry
$_['entry_status']                 = 'Estado';
$_['entry_sort_order']             = 'Ordenar pedidos';
$_['entry_width']                  = 'Anchura';

// Error
$_['error_permission']             = 'Advertencia: no tiene permiso para modificar la actividad del panel de control!';